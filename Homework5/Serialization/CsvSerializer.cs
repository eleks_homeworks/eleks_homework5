﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Homework5.Exceptions;
using Homework5.Settings;

namespace Homework5.Serialization
{
    class CsvSerializer<T> : ISerializer<T> where T : class, new()
    {
        public char Separator { get; set; }
        private readonly List<PropertyInfo> _properties;
        private readonly IFileHelper _helper;

        public CsvSerializer(IFileHelper helper)
        {
            _helper = helper;
            Separator = ',';

            var type = typeof(T);
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance 
                | BindingFlags.GetProperty | BindingFlags.SetProperty);

            _properties = (from a in properties
                           where a.GetCustomAttribute<CsvIgnoreAttribute>() == null
                           orderby a.Name
                           select a).ToList();
        }

        public void Serialize(T data)
        {
            var builder = new StringBuilder();
            var values = new List<string>();

            if (!_helper.IsFileExists)
            {
                _helper.WriteData(GetHeader());
            }
            if (_helper.IsFileExists && _helper.IsFileEmpty)
            {
                _helper.WriteData(GetHeader());
            }

            values.Clear();

            values.AddRange(_properties.Select(p => p.GetValue(data))
                .Select(raw => raw?.ToString() ?? ""));
            builder.AppendLine(string.Join(Separator.ToString(), values.ToArray()));
            
            _helper.WriteData(builder.ToString());
        }

        public IList<T> Deserialize()
        {
            string[] columns;
            string[] rows;

            try
            {
                columns = _helper.ReadColumns(Separator);
                rows = _helper.ReadRows();
            }
            catch (Exception exception)
            {
                throw new InvalidCsvFormatException(
                        "The CSV File is Invalid. See Inner Exception for more inoformation.", exception);
            }

            var data = new List<T>();
            for (var row = 1; row < rows.Length - 1; row++)
            {
                var line = rows[row];
                if (string.IsNullOrWhiteSpace(line))
                {
                    throw new InvalidCsvFormatException($@"Error: Empty line at line number: {row}");
                }

                var parts = line.Split(Separator);

                var item = new T();
                for (var i = 0; i < parts.Length; i++)
                {
                    var value = parts[i];
                    var column = columns[i];
                    
                    var p = _properties.First(a => a.Name == column);

                    var converter = TypeDescriptor.GetConverter(p.PropertyType);
                    var convertedvalue = converter.ConvertFrom(value);

                    p.SetValue(item, convertedvalue);
                }
                data.Add(item);
            }
            return data;
        }

        private string GetHeader()
        {
            var columns = _properties.Select(a => a.Name).ToArray();
            var header = string.Join(Separator.ToString(), columns);
            return header;
        }
    }
}
