﻿using System;
using System.IO;
using Homework5.Settings;

namespace Homework5.Serialization
{
    internal class CsvFileHelper : IFileHelper
    {
        public bool IsFileExists => new FileInfo(UserSettingsHelper.Instance.FilePath).Exists;

        public bool IsFileEmpty => new FileInfo(UserSettingsHelper.Instance.FilePath).Length == 0;

        public void WriteData(string data)
        {
            using (var writer = new StreamWriter(UserSettingsHelper.Instance.FilePath, true))
            {
                writer.Write($"{data.Trim()}{Environment.NewLine}");
            }
        }

        public string[] ReadColumns(char separator)
        {
            string[] columns;
            using (var reader = new StreamReader(new FileStream(UserSettingsHelper.Instance.FilePath, FileMode.Open)))
            {
                columns = reader.ReadLine().Split(separator);
            }
            return columns;
        }

        public string[] ReadRows()
        {
            string[] rows;
            using (var reader = new StreamReader(new FileStream(UserSettingsHelper.Instance.FilePath, FileMode.Open)))
            {
                rows = reader.ReadToEnd().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }
            return rows;
        }
    }
}
