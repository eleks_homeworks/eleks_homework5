﻿namespace Homework5.Model
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        public string Job { get; set; }

        public override string ToString() => $"{FirstName} {LastName}, {Age}, {Job}";
    }
}
