﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Homework5.Model;
using Homework5.Serialization;
using Homework5.View;
using Ninject;

namespace Homework5
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IFileHelper helper = new CsvFileHelper();
            ISerializer<User> serializer = new CsvSerializer<User>(helper);

            Application.Run(new UserLayer(serializer));
        }
    }
}
