﻿namespace Homework5.Serialization
{
    public interface IFileHelper
    {
        bool IsFileExists { get; }
        bool IsFileEmpty { get; }
        void WriteData(string data);
        string[] ReadColumns(char separator);
        string[] ReadRows();
    }
}