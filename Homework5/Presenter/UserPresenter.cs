﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Homework5.Model;
using Homework5.Serialization;
using Homework5.Settings;
using Homework5.View;

namespace Homework5.Presenter
{
    internal class UserPresenter
    {
        private readonly IUserLayer _view;
        private readonly ISerializer<User> _serializer;

        public UserPresenter(IUserLayer view, ISerializer<User> serializer)
        {
            _view = view;
            _serializer = serializer;
            Initialize();
        }

        private void Initialize()
        {
            _view.FirstName = string.Empty;
            _view.LastName = string.Empty;
            _view.Age = string.Empty;
            _view.Job = string.Empty;
            _view.Status = "Ready";
            _view.ReadedData = string.Empty;
            _view.SaveUser += SaveUser;
            _view.ChangeFilePath += ChangeFilePath;
            _view.ReadUsers += ReadUsers;
        }

        private void SaveUser(object sender, EventArgs e)
        {
            var user = new User
            {
                FirstName = _view.FirstName,
                LastName = _view.LastName,
                Age = _view.Age,
                Job = _view.Job
            };
            // Serialize to file
            _serializer.Serialize(user);

            UpdateStatus(user);
        }

        private void UpdateStatus(User user)
        {
            _view.Status = $"{DateTime.Now.Hour}:{DateTime.Now.Minute} added new user \'{user}\'";
        }

        private void ChangeFilePath(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Filter = @"All Files (*.csv)|*.csv",
                FilterIndex = 1,
                Multiselect = false
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                UserSettingsHelper.Instance.FilePath = dialog.FileName;

                _view.FilePath = UserSettingsHelper.Instance.FilePath;
            }
        }

        private void ReadUsers(object sender, EventArgs e)
        {
            // Deserialize from file
            var readedUsers = _serializer.Deserialize().ToList();

            var builder = new StringBuilder();
            for (var i = readedUsers.Count - 1; i >= readedUsers.Count - 10 && i >= 0; i--)
            {
                builder.AppendLine($"{readedUsers[i]}");
            }

            _view.ReadedData = builder.ToString();
        }
    }
}
