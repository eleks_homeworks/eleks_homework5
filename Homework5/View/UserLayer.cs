﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Homework5.Model;
using Homework5.Presenter;
using Homework5.Serialization;
using Homework5.Settings;

namespace Homework5.View
{
    public partial class UserLayer : Form, IUserLayer
    {
        private UserPresenter _presenter;
        private readonly ISerializer<User> _serializer;

        public string FirstName
        {
            get { return firstNameTextBox.Text; }
            set { firstNameTextBox.Text = value; }
        }

        public string LastName
        {
            get { return lastNameTextBox.Text; }
            set { lastNameTextBox.Text = value; }
        }

        public string Age
        {
            get { return ageTextBox.Text; }
            set { ageTextBox.Text = value; }
        }

        public string Job
        {
            get { return jobTextBox.Text; }
            set { jobTextBox.Text = value; }
        }

        public string Status
        {
            get { return statusTextBox.Text; }
            set { statusTextBox.Text = value; }
        }

        public string FilePath
        {
            get { return filePathTextBox.Text; }
            set { filePathTextBox.Text = value; }
        }

        public string ReadedData
        {
            get { return readedDataTextBox.Text; }
            set { readedDataTextBox.Text = value; }
        }

        public UserLayer(ISerializer<User> serializer)
        {
            InitializeComponent();
            _serializer = serializer;
        }

        public event EventHandler<EventArgs> SaveUser;
        public event EventHandler<EventArgs> ChangeFilePath;
        public event EventHandler<EventArgs> ReadUsers;

        private void filePathButton_Click(object sender, EventArgs e)
        {
            // Change File Path
            ChangeFilePath?.Invoke(sender, e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _presenter = new UserPresenter(this, _serializer);
            // Restore File Path from Settings
            FilePath = UserSettingsHelper.Instance.FilePath;
        }

        private void saveUserButton_Click(object sender, EventArgs e)
        {
            //TODO: Add validation
            if (IsFirstNameValid(FirstName) &&
                IsLastNameValid(LastName) &&
                IsAgeValid(Age) &&
                IsJobValid(Job))
            {
                // Save new User
                SaveUser?.Invoke(sender, e);
            }
            else
            {
                MessageBox.Show(this, @"Invalid user");
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void readDataButton_Click(object sender, EventArgs e)
        {
            ReadUsers?.Invoke(sender, e);
        }

        private bool IsFirstNameValid(string firstName)
        {
            var regex = new Regex(@"[a-z]", RegexOptions.IgnoreCase);
            return regex.IsMatch(firstName) && firstName.Length < 50;
        }

        private bool IsLastNameValid(string lastName)
        {
            var regex = new Regex(@"[a-z]", RegexOptions.IgnoreCase);
            return regex.IsMatch(lastName) && lastName.Length < 100;
        }

        private bool IsAgeValid(string age)
        {
            var regex = new Regex(@"[0-300]", RegexOptions.IgnoreCase);
            return regex.IsMatch(age);
        }

        private bool IsJobValid(string job)
        {
            return job.Length <= 255;
        }
    }
}
