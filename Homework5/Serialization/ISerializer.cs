﻿using System.Collections.Generic;

namespace Homework5.Serialization
{
    public interface ISerializer<T> where T : class, new()
    {
        char Separator { get; set; }
        
        void Serialize(T data);
        IList<T> Deserialize();
    }
}