﻿using Homework5.Model;
using Homework5.Serialization;

namespace Homework5.DependencyInjection
{
    class NinjectBindings : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IFileHelper>().To<CsvFileHelper>();
            Bind<ISerializer<User>>().To<CsvSerializer<User>>();
        }
    }
}
