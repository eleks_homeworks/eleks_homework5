﻿using System;

namespace Homework5.Exceptions
{
    class InvalidCsvFormatException : Exception
    {
        public InvalidCsvFormatException(string message) : base(message) { }
        public InvalidCsvFormatException(string message, Exception exception) : base(message, exception) { }
    }
}
