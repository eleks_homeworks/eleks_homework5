﻿using System;

namespace Homework5.View
{
    interface IUserLayer
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string Age { get; set; }
        string Job { get; set; }
        string Status { get; set; }
        string FilePath { get; set; }
        string ReadedData { get; set; }

        event EventHandler<EventArgs> SaveUser;
        event EventHandler<EventArgs> ChangeFilePath;
        event EventHandler<EventArgs> ReadUsers;
    }
}
