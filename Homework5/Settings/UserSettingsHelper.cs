﻿namespace Homework5.Settings
{
    class UserSettingsHelper
    {
        private static UserSettingsHelper _instance;

        public string FilePath
        {
            get { return UserSettings.Default.FilePath; }
            set
            {
                UserSettings.Default.FilePath = value;
                UserSettings.Default.Save();
            }
        }

        private UserSettingsHelper() { }

        public static UserSettingsHelper Instance => 
            _instance ?? (_instance = new UserSettingsHelper());
    }
}
